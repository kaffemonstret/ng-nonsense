(function() {
    'use strict';

    angular
        .module('app.starwars')
        .controller('StarWarsController', StarWarsController);

    StarWarsController.$inject = ['starwars'];

    function StarWarsController(starwars) {
        var vm = this;
        vm.getMore = getMorePeople;
        vm.loading = false;
        vm.offset = 0;
        vm.people = [];

        getPeople();

        function getPeople() {
            vm.loading = true;
            starwars.people(vm.offset).then(function(data) {
                vm.people = vm.people.concat(data);
                vm.loading = false;
                console.log('Finished loading');
            });
            console.log('Not finished loading');
        }

        function getMorePeople() {
            vm.offset++;
            getPeople();
        }
    }
})();
