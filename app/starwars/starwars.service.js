(function() {
    'use strict';

    angular
        .module('app.starwars')
        .factory('starwars', starwars);

    starwars.$inject = ['$http'];

    function starwars($http) {
        var service = {
            people: people
        };

        return service;

        function people(offset) {
            return $http.get('http://swapi.co/api/people/?page=' + (offset + 1)).then(
                function(data) {
                    if (data.status !== 200) {
                        console.log('Could not retrieve data');
                    }
                    // This is not particulary safe...
                    return data.data.results;
                }
            ).catch(function(data) {
                console.log('$http.get error.');
                return [];
            });
        }
    }
})();
