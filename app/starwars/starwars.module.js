(function() {
    'use strict';

    angular
        .module('app.starwars', ['ngRoute', '$http']);
})();
