(function() {
    'use strict';

    angular
        .module('app.starwars', ['ngRoute'])
        .config(configure);

        configure.$inject = ['$routeProvider'];

        function configure($routeProvider) {
            $routeProvider
                .when('/starwars', {
                    templateUrl: 'starwars/starwars.html',
                    controller: 'StarWarsController',
                    controllerAs: 'starwars'
                });
        }
})();
