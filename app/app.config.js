(function() {
    'use strict';

    angular
        .module('app')
        .config(configure);

        configure.$inject = ['$locationProvider'];

        function configure($locationProvider) {
            //
            // Setting the HTML5 mode on the $locationProvider removes the
            // use of # in the URL's. And we can use absolute paths and exlude
            // the # from our href's as well.
            //
            // E.g. href="#basic/expression" => href="/basic/expression"
            //
            // This however require some server side rewrites on URL's to
            // index.html
            // $locationProvider.html5Mode(true);
        }
})();
