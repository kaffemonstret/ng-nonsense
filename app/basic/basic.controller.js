(function() {
    'use strict';

    angular
        .module('app')
        .controller('BasicController', BasicController);

    BasicController.$inject = [];

    function BasicController() {
        /**
         * The members is alphabetized and in the top
          *https://github.com/johnpapa/angular-styleguide#style-y033
         */
        var vm = this;
        vm.alphabet = ['alpha', 'bravo', 'charlie', 'delta', 'echo', 'foxtrot'];
        vm.ticker = 0;
        vm.incrementTicker = incrementTicker;

        function incrementTicker() {
            vm.ticker++;
        }
    }
})();
