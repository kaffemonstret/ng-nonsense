(function() {
    'use strict';

    angular
        .module('app')
        .controller('BackofficeController', BackofficeController);

    BackofficeController.$inject = ['signup'];

    function BackofficeController(signup) {
        var vm = this;
        vm.registrations = signup.registrations;
    }
})();
