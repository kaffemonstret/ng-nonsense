(function() {
    'use strict';

    angular
        .module('app')
        .controller('SignupController', SignupController);

    SignupController.$inject = ['signup'];

    function SignupController(signup) {
        var vm = this;
        vm.reset = reset;
        vm.signupUser = signupUser;

        function reset() {
            vm.userForm.name = '';
            vm.userForm.$setPristine();
        }

        function signupUser() {
            if (vm.userForm.$valid) {
                signup.register(vm.userForm.name);
                reset();
            }
        }
    }
})();
