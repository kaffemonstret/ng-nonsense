(function() {
    'use strict';

    angular
        .module('app.signup')
        .factory('signup', Signup);

    Signup.$inject = [];

    function Signup() {
        var registrations = [];

        var service = {
            register: register,
            registrations: registrations
        };

        return service;

        function register(name) {
            registrations.push(name);
        }
    }
})();
