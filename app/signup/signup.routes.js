(function() {
    'use strict';

    angular
        .module('app.signup', ['ngRoute'])
        .config(configure);

        configure.$inject = ['$routeProvider'];

        function configure($routeProvider) {
            $routeProvider
                .when('/signup', {
                    templateUrl: 'signup/signup.html',
                    controller: 'SignupController',
                    controllerAs: 'signup'
                })
                .when('/signup/backoffice', {
                    templateUrl: 'signup/backoffice.html',
                    controller: 'BackofficeController',
                    controllerAs: 'backoffice'
                });
        }
})();
