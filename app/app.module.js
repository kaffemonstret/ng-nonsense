(function() {
    'use strict';

    angular
        .module('app', ['app.signup', 'app.starwars', 'ngRoute'])
        .config(configure);

        configure.$inject = ['$routeProvider'];

        function configure($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'landing/landing.html',
                })
                .when('/basic', {
                    templateUrl: 'basic/basic.html'
                })
                .when('/basic/expression', {
                    templateUrl: 'basic/expression.html',
                    controller: 'BasicController',
                    controllerAs: 'basic'
                })
                .when('/basic/controller', {
                    templateUrl: 'basic/controller.html',
                    controller: 'BasicController',
                    controllerAs: 'basic'
                })
                .when('/basic/looping', {
                    templateUrl: 'basic/looping.html'
                })
                .when('/basic/directive', {
                    templateUrl: 'basic/directive.html',
                    controller: 'BasicController',
                    controllerAs: 'basic'
                })
                .when('/basic/scope', {
                    templateUrl: 'basic/scope.html'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
})();
