(function() {
    'use strict';

    angular
        .module('app')
        .directive('enterSomething', enterSomething)
        .directive('enterSomethingIsolated', enterSomethingIsolated);

    function enterSomething() {
        return {
            restrict: 'E',
            template: '<input type="text" ng-model="something"/> {{ something }}<br/>'
        };
    }

    function enterSomethingIsolated() {
        return {
            restrict: 'E',
            scope: {},
            template: '<input type="text" ng-model="something"/> {{ something }}<br/>'
        };
    }
})();
