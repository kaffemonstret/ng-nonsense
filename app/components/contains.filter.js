(function() {
    'use strict';

    angular
        .module('app')
        .filter('contains', contains);

    /**
     * A filter that filters a list of strings based on they contain the given
     * character or not.
     */
    function contains() {
        return function(list, c) {
            var result = [];
            angular.forEach(list, function(word) {
                if (word.indexOf(c) > -1) {
                    result.push(word);
                }
            });
            return result;
        };
    }
})();
