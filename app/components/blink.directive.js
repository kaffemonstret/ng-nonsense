(function() {
    'use strict';

    angular
        .module('app')
        .directive('blink', blink);

    function blink() {
        return {
            /**
             * This directive is intended to be used as a separate
             * element or as an attribute.
             */
            restrict: 'EA',

            /**
             * Replace the directive with the template given below.
             */
            replace: true,

            /**
             * Inject any passed DOM-elements to the directive.
             */
            transclude: true,

            /**
             * The template rendering this directive, it should always use the
             * same name as the directive.
             */
            templateUrl: 'components/blink.directive.html'
        };
    }
})();
