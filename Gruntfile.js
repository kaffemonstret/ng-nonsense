'use strict';

module.exports = function (grunt) {
    // This will automatically load all added dependencies
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        /**
         * Starts the local server serving the static content (angular app)
         */
        connect: {
            options: {
                port: 9000,
                hostname: 'localhost',
                keepalive: true
            },
            server: {
                options: {
                    base: 'app'
                }
            },
            dist: {
                options: {
                    base: 'dist'
                }
            }
        },

        /**
         * This will clean any generated temporary files and any generated dist
         */
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        'dist/*',
                        '!dist/.git*'
                    ]
                }]
            }
        },

        /**
         * Copy files from source app to dist
         */
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'app/',
                    dest: 'dist/',
                    src: [
                        '*.html',
                        '*.js',
                        '*.{ico,png,txt}',
                        'bower_components/**/*',
                        'images/{,*/}*.{gif,webp}'
                    ]}
                ]
            },
            styles: {
                expand: true,
                cwd: 'app/stylesheets/',
                dest: 'dist/stylesheets',
                src: ['*.css']
            }
        },

        /**
         * Runs jshints all JavaScript code (including this file)
         */
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'app/!bower_components/{,*/}*.js'
            ]
        }
    });

    grunt.registerTask('server', [
        'clean:dist',
        'copy:dist',
        'copy:styles',
        'connect:server',
    ]);

    grunt.registerTask('test', [
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'copy:dist',
        'copy:styles'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'test',
        'build'
    ]);
};
