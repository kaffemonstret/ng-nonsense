# ng-nonsense

ng-nonsense is a toy project to illustrate some of AngularJs features.


## Running

Run these three commands to install all needed dependencies and spin
up the development server.

    # This will install all dependencies needed for the toolchain and
    # store them in the directory node_modules.
    # E.g. grunt, etc (packages listed in package.json)

    $ npm install

    # This will install the dependencies used by the angular web
    # application into the directory app/bower_components

    $ bower install


    # This will start a local web server to serve the static files
    # constituting the angular application.

    $ grunt server


You should now have a server up and running at:

http://localhost:9000


## TODOS

- Example code for futures (fn, closures)
- Example code for prototypes
- Example code for a simple overview alt. a illustration
- Motivate in slides the usage of vm and not scope

